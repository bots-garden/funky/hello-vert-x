# Hello Vert-x

> WIP 🚧

> WIP 🚧

```bash
echo -n USER:PASSWORD | base64
{
  "auths": {
    "https://index.docker.io/v1/": {
      "auth": "xxxxxxxxxxxxxxx"
    }
  }
}

export KUBECONFIG=../../cluster/k3s.yaml

kubectl create namespace funky-demo-prod

kubectl create configmap docker-config -n funky-demo-prod --from-file=./config.json
kubectl get configmap docker-config -n funky-demo-prod
kubectl describe configmap docker-config -n funky-demo-prod

kubectl delete namespace funky-demo-prod
```
